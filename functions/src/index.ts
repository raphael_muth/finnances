import './Prototypes'; //precisa ser o primeiro
import './loads/LoadAdmin'; //precisa ser o primeiro ou quase
import "reflect-metadata";
import { https } from 'firebase-functions';
import { Server } from './loads/Server';

const server =  new Server();
exports.api = https.onRequest(server.app);

