// src/resolvers/FolderResolver.ts

import { Query, Resolver } from "type-graphql";
import Folder from "../schemas/Folder";
import admin from 'firebase-admin';

const foldersRef = admin.database().ref('folders');

@Resolver(Folder)
export default class {
    @Query(returns => [Folder], { nullable: true })
    folders(): Promise<Folder[]> {
        return foldersRef
            .once('value')
            .then((snapshot: any) => {
                const folders = snapshot.val() || {};
                return Object
                    .keys(folders)
                    .map(o => Object.assign({ id: o }, folders[o]))
                    .setDateInstances("date");
            });
    }
}