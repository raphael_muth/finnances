import FolderResolver from "../resolvers/FolderResolver";
import EntryResolver from "../resolvers/EntryResolver";
import { buildSchemaSync  } from "type-graphql";
import { GraphQLSchema } from "graphql";

class SchemaBuider {
    public buildedSchema: GraphQLSchema
    constructor() { this.build(); }

    build() {
        this.buildedSchema = buildSchemaSync({
            resolvers: [FolderResolver, EntryResolver], 
            dateScalarMode: "isoDate",
            emitSchemaFile: true
        })
    }
}


export const Buider = new SchemaBuider();