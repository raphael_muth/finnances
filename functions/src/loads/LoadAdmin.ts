import admin from 'firebase-admin'; 
var serviceAccount = require("../../serviceAccountKey.json");
 
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://finnances-6bda9.firebaseio.com"
});

export {}