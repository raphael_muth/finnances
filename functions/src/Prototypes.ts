declare global {
    interface Array<T> {
        setDateInstances(field: string): Array<T>;
    }
}

if (!Array.prototype.setDateInstances) {
    Array.prototype.setDateInstances = function (field) {
        this.forEach(x => {
            if( x[field] ) x[field] = new Date ( x[field] )
        } )
        return this;
    }
}

export { }