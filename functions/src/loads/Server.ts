
import express from 'express';
import { Express } from 'express-serve-static-core';
import cors from 'cors';
import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import { printSchema } from 'graphql/utilities/schemaPrinter';
import { Buider } from '../loads/BuildSchema';

export class Server {
    public app: Express; 
    
    private setHeader(req: any, res: any, next: any) {
        res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
        return next();
    }

    private printSchema(req: any, res: any) {
        res.set('Content-Type', 'text/plain');
        res.send( printSchema( Buider.buildedSchema ) );
    }

    constructor() {
        this.app = express();
        this.app.use(cors());
        this.app.options('*', cors());
        this.app.use( this.setHeader ); 
        this.app.use(EndPoints.graphqlEndPoint, graphqlExpress({ schema: Buider.buildedSchema }));
        this.app.use(EndPoints.graphiqlEndPoint, graphiqlExpress({ endpointURL: `${EndPoints.functionsEndPointPrefix}${EndPoints.graphqlEndPoint}` }));
        this.app.use(EndPoints.schemaEndPoint, this.printSchema);
    }

}

export enum EndPoints {
    graphqlEndPoint = '/graphql',
    graphiqlEndPoint = '/graphiql',
    schemaEndPoint = '/schema',
    functionsEndPointPrefix = '/finnances-6bda9/us-central1/api'
}