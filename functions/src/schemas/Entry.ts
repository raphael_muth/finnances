
import { Field, Float, ObjectType  } from "type-graphql"; 

@ObjectType()
export default class Entry { 
    @Field(type => Float)
    amount: number;

    @Field( )
    date: Date;
    
    @Field()
    category: string;
    
    @Field()
    title: string;
}