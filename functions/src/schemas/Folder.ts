import { Field,  ObjectType, GraphQLISODateTime } from "type-graphql"; 

@ObjectType()
export default class Folder {
    @Field( )
    id: string;

    @Field()
    name: string; 

    @Field( )
    date: Date;
} 