// src/resolvers/EntryResolver.ts

import { Query, Resolver, Arg } from "type-graphql";
import Entry from "../schemas/Entry";
import admin from 'firebase-admin'; 

const entriesRef = admin
  .database()
  .ref('entries')
  .orderByChild('date');

@Resolver(of => Entry)
export default class {
  @Query(returns => [Entry], { nullable: true })
  entries(): Promise<Entry[]> {
    return entriesRef
      .once('value')
      .then(snapshot => ((snapshot.val() || []) as Entry[]).setDateInstances("date"));
  }
  @Query(returns => [Entry], { nullable: true })
  entries_by_date(
    @Arg("initialDate") initialDate: Date,
    @Arg("endDate", { nullable: true }) endDate?: Date,
    @Arg("count", { nullable: true }) count?: number): Promise<Entry[]> {
    var ref = entriesRef.startAt(initialDate.toISOString());
    if (endDate) ref = ref.endAt(endDate.toISOString());
    if (count) ref = ref.limitToFirst(count);
    
    return ref
      .once('value')
      .then(snapshot => {
        var data = this.parseObjectToArray<Entry>((snapshot.val() || []));
        console.log(data);
        return data.setDateInstances("date");
      });
  } 

  private parseObjectToArray<T>(data: any) : T[]{
    return Object
      .keys(data)
      .map(i => Object.assign({}, data[i] ) );
  }
  
  private retrieveOnce<T>(ref: admin.database.Query): Promise<T[]> {
    return ref
      .once('value')
      .then(snapshot => {
        var data: T[] = snapshot.val() || [];
        return data;
      });
  }
}